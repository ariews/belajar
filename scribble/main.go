package main

import (
	"fmt"

	scribble "github.com/nanobox-io/golang-scribble"
)

// Book ...
type Book struct {
	Title    string
	Chapters []Chapter
}

func (b *Book) AddChapter(c Chapter) {
	b.Chapters = append(b.Chapters, c)
}

func (b *Book) SetChapters(cs []Chapter) {
	b.Chapters = cs
}

// Chapter ...
type Chapter struct {
	Word int
}

func main() {
	dir := "data"
	db, err := scribble.New(dir, nil)
	if err != nil {
		fmt.Println("err", err)
	}

	book := Book{Title: "Book 1"}
	chapters := make([]Chapter, 0)
	for _, c := range []int{23, 34, 44, 56} {
		chapters = append(chapters, Chapter{
			Word: c,
		})
	}
	book.SetChapters(chapters)

	if err := db.Write("book", "onebook", book); err != nil {
		fmt.Println("err", err)
	}

	oldBook := Book{}
	if err := db.Read("book", "onebook", &oldBook); err != nil {
		fmt.Println("err", err)
	}

	oldBook.AddChapter(Chapter{Word: 345})

	if err := db.Write("book", "onebook", oldBook); err != nil {
		fmt.Println("err", err)
	}

	fmt.Println(oldBook)
}
